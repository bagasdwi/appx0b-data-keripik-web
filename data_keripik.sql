-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Bulan Mei 2020 pada 12.11
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_keripik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `keripik`
--

CREATE TABLE `keripik` (
  `id_keripik` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `id_rasa` int(11) NOT NULL,
  `harga` varchar(20) NOT NULL,
  `photos` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `keripik`
--

INSERT INTO `keripik` (`id_keripik`, `nama`, `id_rasa`, `harga`, `photos`) VALUES
('1', 'Keripik Pisang', 1, '15000', 'pisang.jpg'),
('2', 'Keripik Singkong', 2, '10000', 'singkong.jpg'),
('3', 'Keripik Kentang', 3, '12000', 'kentang.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rasa`
--

CREATE TABLE `rasa` (
  `id_rasa` int(11) NOT NULL,
  `nama_rasa` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rasa`
--

INSERT INTO `rasa` (`id_rasa`, `nama_rasa`) VALUES
(1, 'Original'),
(2, 'Pedas'),
(3, 'Barbeque');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `keripik`
--
ALTER TABLE `keripik`
  ADD PRIMARY KEY (`id_keripik`);

--
-- Indeks untuk tabel `rasa`
--
ALTER TABLE `rasa`
  ADD PRIMARY KEY (`id_rasa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
