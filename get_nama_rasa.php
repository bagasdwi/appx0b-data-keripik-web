<?php 

    $DB_NAME = "data_keripik";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama_rasa = $_POST['nama_rasa'];
        if(empty($nama_rasa)){
            $sql = "select * from rasa order by nama_rasa asc";
        }else{
            $sql = "select * from rasa where nama_rasa like '%$nama_rasa%' order by nama_rasa asc";
        }
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_rasa = array();
            while($nm_rasa = mysqli_fetch_assoc($result)){
                array_push($nama_rasa,$nm_rasa);
            }
            echo json_encode($nama_rasa);
        }
    }