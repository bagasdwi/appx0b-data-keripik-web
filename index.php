<?php
    $DB_NAME = "data_keripik";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT k.id_keripik, k.nama, r.nama_rasa, k.harga, k.photos
            FROM keripik k, rasa r 
            WHERE k.id_rasa = r.id_rasa";
    
    $result = mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link rel="stylesheet" href="/kampus2/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <strong>
                <h3>Data Mahasiswa</h3>
            </strong>
            <table class="mt-3 table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nama</th>
                        <th>Rasa</th>
                        <th>harga</th>
                        <th>Foto</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <?php 
                    while($krp = mysqli_fetch_assoc($result)){
                ?>
                <tbody>
                    <tr>                 
                        <td><?php echo $krp['id_keripik']; ?></td>
                        <td><?php echo $krp['nama']; ?></td>
                        <td><?php echo $krp['nama_rasa']; ?></td>
                        <td><?php echo $krp['harga']; ?></td>
                        <td><img src="images/<?php echo $krp['photos']; ?>" style="width: 100px;"></td>
                        <td><a onclick="return confirm('Apakah yakin data akan di hapus?')" href="" class="btn btn-danger btn-sm">Hapus</span></td>
                    </tr> 
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>