<?php

$DB_NAME = "data_keripik";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $nama = $_POST['nama'];
    $sql = "SELECT k.id_keripik, k.nama, r.nama_rasa, k.harga, concat('http://192.168.43.94/data_keripik/images/',k.photos) as url
            FROM keripik k, rasa r 
            WHERE k.id_rasa = r.id_rasa
            and k.nama like '%$nama%'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_krp = array();
        while ($krp = mysqli_fetch_assoc($result)) {
            array_push($data_krp, $krp);
        }
        echo json_encode($data_krp);
    }
}
